
/*
	fetch() is a method in JS, which allows to send request tp an api and process its response. fetch has 2 arguments, the url to resource/route and optional object which contains additional information about our requests such as method, the contents of our body, and the headers of our request.

	fetch () method
	Syntax: 
		fetch (url, options)
*/ 

// Get post data using fetch()
// Nilalagay kapag connect front end to back end
// Kapag get method lang, hindi na kailangan ispecify yung method sa request. 
fetch('https://jsonplaceholder.typicode.com/posts')
// I asign yung mafefetch into response then convert into a json object
.then((response) => response.json())
// I assign kay data then galing kay data, ibigay kay console.log
.then((data) => console.log(data))


// Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST', 
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value, 
			userId: 1
		}),
		headers: {
			'Content-type': 'application/jason; charset=UTF-8'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		
		console.log(data);
		alert("Successfully Added.");


	// Clear the text elements upon post creation
	document.querySelector('#txt-title').value = null;
	document.querySelector('#txt-body').value = null;
	})

})

const showPosts = (posts) => {
	let postEntries = ''
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Mini Activity: Get or retrieve all the posts. Use show post function
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => {
	showPosts(data)
})

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	// Removes the disables attribute from the button
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

// Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT', 
		body: JSON.stringify({

			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1

		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}

	}).then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('Successfully updated')

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null; 
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
})

/*
	Mini Activity: 
		Retrieve a single post and print it in the console.
*/ 
fetch('https://jsonplaceholder.typicode.com/posts/89')
.then((response) => response.json())
.then((data) => {
	console.log(data)
})

// Delete Post
const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELETE'
	});
	document.querySelector(`#post-${id}`).remove();
}


